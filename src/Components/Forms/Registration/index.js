import './index.scss';
import { useState } from 'react';
import logo from '../../../Assets/images/logo-white.png'
import HomeHeader from '../../Header/HomeHeader';
const Registration = () => {

    const [placeHolder, setPlaceHolder] = useState({
        sport: true,
        package: true,
        county: true
    });
    const changeHandler = (e) => {
        if (e.target.value !== '') {
            setPlaceHolder({
                ...placeHolder,
                [e.target.name]: false
            })
        }
    }
    return (
        <div>
            <HomeHeader />
            <form className="contactForm">
                <img src={ logo } alt="sportStream" />
                <h4>Personal detials</h4>
                <div>
                    <input type="text" name="firstName" placeholder="First Name" />
                    <input type="text" name="surname" placeholder="Surname" />
                </div>
                <input type="text" name="email" placeholder="Email Address" />
                <div>
                    <input type="text" name="password" placeholder="Password" />
                    <input type="text" name="repassword" placeholder="Repeat password" />
                </div>
                <h4>
                    Club Details
                </h4>
                <input type="text" name="club" placeholder="Name of club/team" />
                <select name='sport' style={ placeHolder.sport ? { color: "#999" } : null } onChange={ changeHandler }>
                    <option value="" disabled={ !placeHolder.sport }>Sport</option>
                    <option value="cricket">Cricket</option>
                    <option value="rugby">Rugby</option>
                    <option value="football">Football</option>
                </select>
                <button>Submit</button>
            </form>
        </div>

    );
}

export default Registration;