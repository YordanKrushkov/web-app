import gold from '../Assets/images/Gold-Medal.png';
import silver from '../Assets/images/Silver-Medal.png';
import bronze from '../Assets/images/Bronze-Medal.png';

export default [
    {
        package: "Gold",
        medal: gold,
        p: [
            "Full range of remotely controlled cameras with multiple angles and flexible zoom, for all home games",
            "Live streaming directly to the Sport Stream app and website, and up to two other requested platforms such as Facebook and YouTube. Live stream replay also accessible via the app",
            "Full gold graphics package with club badges, running scores, live updates, instant replays and game clock. Space for sponsorship icons and logos",
            "Full highlights packages available same-day on the Sport Stream app and website, for general and play-specific highlights. Option available to use and post highlights to club's channels as and when you wish",
            "Commentary feature optional for club commentators during the live stream",
            "Highlights automatically entered in to the Sport Stream Highlight of the Week feature, with the winner shared across our website and social media channels",
        ]
    },
    {
        package: "Silver",
        medal: silver,
        p: [
            "Central remotely controlled cameras with multiple angles and flexible zoom, for all home games",
            "Live streaming directly to the Sport Stream app and website, and up to two other requested platforms such as Facebook and YouTube. Live stream replay also accessible via the app",
            "Silver graphics package with club badges, running scores, live updates, instant replays and game clock.",
            "Full highlights packages available next-day on the Sport Stream app and website, for general and play-specific highlights.",
            "Commentary feature optional for club commentators during the live stream",
            "Highlights automatically entered in to the Sport Stream Highlight of the Week feature, with the winner shared across our website and social media channels",
        ]
    },
    {
        package: "Bronze",
        medal: bronze,
        p: [
            "Central remotely controlled cameras with multiple angles and flexible zoom, for all home games",
            "Live streaming directly to the Sport Stream app and website, and full stream replay available post-match",
            "Bronze graphics package with club badges, running scores, live updates, instant replays and game clock.",
            "Highlights packages available on request (with notice) for an additional one-off charge and will be made accessible via the Sport Stream app and website",
            "Highlights automatically entered in to the Sport Stream Highlight of the Week feature, with the winner shared across our website and social media channels",
        ]
    },
]