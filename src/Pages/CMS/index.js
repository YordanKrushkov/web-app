import './index.scss';
import LoginForm from '../../Components/Login'
import {useContext} from 'react';
import { AdminAuthContext } from '../../Context/adminsAuht';
import auth from '../../Services/auth'
import AdminLayout from '../../Components/CMS/Layout';
const CMS = () => {
    const {login,isAuth}=useContext(AdminAuthContext)
    console.log(isAuth);
    const submitHandler = (e) => {
        e.preventDefault();

        auth('/adminlogin', {
          email: e.target.email.value,
          password: e.target.password.value
        }, login,'admin')
          .then()
          .catch(er=>console.log(er))
    }
    return (
        !isAuth?
        <LoginForm
            submitHandler={ submitHandler }
            text={ "SportStream admin page" }
        />
        :<AdminLayout/>
    );
}

export default CMS;