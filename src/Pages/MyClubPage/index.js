import './index.scss';
import Header from '../../Components/Header/Header';
import logo from '../../Assets/images/Reigate-Priory-Logo.png'
import MoreVideos from '../../Components/MoreVideos'
import Footer from '../../Components/Footer'
import { FaPlay } from "react-icons/fa";
import ReactPlayer from 'react-player'
import { useState, useEffect } from 'react'
const MyClubPage = () => {
    const [play, setPlay] = useState(false);
    const [id, setId] = useState('pouseMyClubVideo');

    useEffect(() => {
        if (play) {
            setId("playMyClubVideo")
        } else {
            setId('pouseMyClubVideo')
        }
        console.log(id);
    }, [play])
    return (
        <div>
            <div id="myClubPageWrapper">
                <Header title="My Club" subTitle="Reigate priory cc" />
                <main>
                    <div>
                        <img src={ logo } alt="" />
                        <div className="clubInfo">
                            <p>The home of your clubs' Sport Stream footage.</p>
                            <p>Catch up on all of your club's fixtures with full match replays, general and specific highlights and of course watch the games live right here, or on the move, via the Sport Stream app.</p>
                        </div>

                    </div>
                    <div id="myPageClubVideo" className={ id }>
                        <div>
                            <ReactPlayer width="600px"  url="http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4"
                                playing={ play }
                                onPause={ () => setPlay(false) }
                                onSeek={ () => setPlay(true) }
                                controls={ play }
                                style={ { border: "none" } }
                            />
                            { !play && <FaPlay onClick={ () => setPlay(true) } /> }
                        </div>
                        <h5>Full match replay</h5>
                    </div>
                    <div id="myPageClubInfo">
                        <h1>Latest club footage</h1>
                        <h2>Reigate Priory vs banstead 28.08.21</h2>
                        <p>Full fixture and videos here</p>
                    </div>
                </main>
                <MoreVideos text={ "More fixtures from reigate priory: " } secondText={ "  Open a fixture below to see all the videos from that match" } />
            </div>
            <div style={ { position: "relative", paddingTop: "100px", overflow: "hidden" } }>
                {/* <TeamLineUp /> */ }
            </div>
        </div>
    );
}

export default MyClubPage;