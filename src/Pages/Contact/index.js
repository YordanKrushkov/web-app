import './index.scss';
import Header from '../../Components/Header/Header'
import {Link} from 'react-router-dom'
import facebook from '../../Assets/images/Facebook Logo.png'
import instagram from '../../Assets/images/Instagram Logo.png'
const Contact = () => {
    return ( 
        <div id="contactWrapper">
            <Header title="Contact" />
            <main>
                <p>General enquiries: info@sport-stream.co.uk</p>
                <p>Sales: jordan@sport-stream.co.uk</p>
                <p>For information about packages and to register your club's interest,<br/> please complete the form on the <Link to="packages">Packages</Link> page</p>
                <div>
                <Link><img src={facebook} alt="facebook"/></Link>
                <Link><img src={instagram} alt="instagram"/></Link>
                </div>
                <p>Follow us on Facebook and Instagram for more content, regular <br/> updates, best bits and extras</p>
            </main>
        </div>
     );
}
 
export default Contact;