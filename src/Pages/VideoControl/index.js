
import './index.scss';
import SVG from '../../Components/SVG';
import React, { useEffect, useRef } from "react";
import Hls from 'hls.js';
import {startStreaming, stopStreaming, zoom,change} from '../../Services/videoServices'
const VideoControl = () => {
    const videoRef = useRef(null);
    const src = "https://stream.mux.com/2JqBoVTW773Xr00nfFQnqUlpfmr5HLdVHn00Od02QXtDCw.m3u8";


    useEffect(() => {
        let hls;
        if (videoRef.current) {
            const video = videoRef.current;

            if (video.canPlayType("application/vnd.apple.mpegurl")) {
                // Some browers (safari and ie edge) support HLS natively
                video.src = src;
            } else if (Hls.isSupported()) {
                // This will run in all other modern browsers
                hls = new Hls();
                hls.loadSource(src);
                hls.attachMedia(video);
            } else {
                console.error("This is a legacy browser that doesn't support MSE");
            }
        }
        return () => {
            if (hls) {
                hls.destroy();
            }
        };
    }, [videoRef]);

    return (<div id="videoControl">
        <div>
            <video
                controls
                ref={ videoRef }
                style={ { width: "100%", maxWidth: "800px" } }
            />
            <div className="wrapper">
                <button onClick={startStreaming}>Start Streaming</button>
                <button onClick={stopStreaming}>Stop Streaming</button>
                <button onClick={change}>Change</button>
                <button onClick={()=>zoom(2)}>Zoom</button>
                <button onClick={()=>zoom(0)}>Zoom Out</button>
            </div>
        </div>

        <SVG />
    </div>);
}

export default VideoControl;