import './index.scss'

const Messages = () => {
    return (
        <div id="messages">
            <header>
                <h1>Sport Stream</h1>
            </header>
            <main>
                <div id="messsage">
                    <div id="leftSideMessage">
                        <h3>Lorem, ipsum dolor.</h3>
                        <h4>Lorem, ipsum.</h4>
                    </div>
                    <div id="rightSideMessage">
                        <h3>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eaque, aspernatur!</h3>
                        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Porro debitis velit quia ipsa sunt corporis in soluta perferendis voluptatum autem dolorum modi quasi, facilis fugit libero culpa, illo esse aliquam quod repudiandae incidunt voluptates blanditiis. Modi earum, est exercitationem possimus explicabo eos dolor quasi velit repellat atque, ab quam iure? Assumenda dolorem labore dolore natus delectus incidunt quia excepturi ut.</p>
                    </div>
                </div>
            </main>

        </div>
    );
}

export default Messages;