import { Component, createContext } from 'react';
import { adminVerify } from '../Services/verify'
export const AdminAuthContext = createContext()

class AdminAuthContextProvider extends Component {
    state = {
        isAuth: false,
        firstName: '',
        surname: '',
        email: '',
    }

    login = (data) => {
        this.setState({
            isAuth: true,
            firstName: data.firstName,
            surname: data.surname,
            email: data.email
        })
    }
    logout = () => {
        document.cookie = "admin-auth-token=";
        this.setState({
            isAuth: false,
            firstName: '',
            surname: '',
            email: ''
        })
    }

    componentDidMount() {
        adminVerify().then((res) => {
            if (res) {
                this.setState({
                    userID: res._id,
                    isAuth: true,
                    email: res.email,
                    firstName: res.firstName,
                    surname: res.surname,
                })
            } else {
                this.logout();
            }
        })
            .catch((err) => console.log(err));
    }

    render() {
        return (
            <AdminAuthContext.Provider value={ { ...this.state, login: this.login, logout: this.logout } }>
                {this.props.children }
            </AdminAuthContext.Provider>
        )
    }

}

export default AdminAuthContextProvider;