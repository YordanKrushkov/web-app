import './index.scss';
import logo from '../../Assets/images/logo-white.png';
import WhatWeDo from '../../Components/WhatWeDo';
import Header from '../../Components/Header/Header'
const About = () => {
    return (
        <div>
            <div id="aboutWrapper">
                <Header title="About" />
                <main>
                    <img src={logo} />
                    <div>

                        <p>Think of the greatest moment of sport that you've ever been a part
                            of...</p>
                        <p>And imagine you could watch it all back again.</p>
                        <p>Sport Stream sets up at amateur and semi professional sports clubs
                            teams, for high quality live streaming. However, we aren't just a
                            tech guy with hand-held camera on the sidelines... Our goal is to
                            provide the opportunity for clubs to experience close to the
                            quality that we see on our screens every week. With the options
                            for multiple controlled cameras, text and graphic overlays, game
                            clock, stats, updates and more, we want to provide a full matchday
                            and post-match experience.
                        </p>
                        <p>After the game, users will be able to watch their match back in
                            full, or through various highlights packages. Our dedicated app
                            and website streams, stores and completes your fixtures, tailored
                            to the club's preferences.
                        </p>
                    </div>

                </main>
            </div>
            <WhatWeDo />
        </div>
    );
}

export default About;