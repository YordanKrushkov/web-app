import './index.scss'
import box from '../../Assets/images/white-box.png'

const HighligntsVideos = ({ setVideo }) => {
    return (
        <div id="higlightsVideosVideosWrapper">
            <h1 className="oneTitle">More of our featured highlights</h1>
            <div>
                <div className='videosWrapper'>
                    <section onClick={setVideo}>
                        <video src="http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4" />
                        <img className="videoBox" src={box} />
                        <p>New Freekick</p>
                    </section>
                    <section onClick={setVideo}>
                        <video src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4" />
                        <img className="videoBox" src={box} />
                        <p>Full match replay</p>
                    </section>
                    <section onClick={setVideo}>
                        <video src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4" />
                        <img className="videoBox" src={box} />
                        <p>Freekick</p>
                    </section>
                    <section onClick={setVideo}>
                        <video src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4" />
                        <img className="videoBox" src={box} />
                        <p>Sensational try</p>
                    </section>
                    <section onClick={setVideo}>
                        <video src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4" />
                        <img className="videoBox" src={box} />
                        <p>Something New</p>
                    </section>
                    <section onClick={setVideo}>
                        <video src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4" />
                        <img className="videoBox" src={box} />
                        <p>Something New</p>
                    </section>
                    <section onClick={setVideo}>
                        <video src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4" />
                        <img className="videoBox" src={box} />
                        <p>Something New</p>
                    </section>
                </div>

            </div>
        </div>
    );
}

export default HighligntsVideos;