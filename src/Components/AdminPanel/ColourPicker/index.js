import Colors from './colorPicker'
import { ThemeContext } from '../../../Context/theme';
import { useContext } from 'react';
const ColorPicker = ({colorSet}) => {

    const { changeStripe, previewColor,resetStripe } = useContext(ThemeContext)
    const clickHandler = () => {
        changeStripe(previewColor);
        colorSet()
    }
    const resentHandler = () => {
        resetStripe();
    }
    return (
        <div id="colorPickerWrapper">
            <Colors />
            <div id="colorButtonWrapper">
            <button onClick={ resentHandler }>Reset Color</button>
            <button onClick={ clickHandler }>Confirm</button>
            </div>

        </div>
    );
}

export default ColorPicker;