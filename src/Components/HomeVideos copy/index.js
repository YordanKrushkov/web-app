import { useState, useEffect } from 'react'
import './index.scss';
import box from '../../Assets/images/white-box.png'
import { FaPlay } from "react-icons/fa";
import ReactPlayer from 'react-player';
const HomeVideos = () => {
    const [play, setPlay] = useState(false);
    const [id, setId] = useState('pauseVideo')
    // let el = document.getElementById('mainVideo');
    useEffect(() => {
        if (play) {
            setId("playVideo")
        } else {
            setId('pauseVideo')
        }
        console.log(id);
    }, [play, id])
    return (
        <div id="homeVideos">
            <h1>Sport stream in action</h1>
            <div>
                <span>
                    <video src="http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4" />
                    <img className="videoBox" src={ box } />
                    <p>Sensational try</p>
                </span>
                <span className="secondVideo">
                    <video src="http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4" />
                    <p>Did she catch that?</p>
                    <img className="secondVideoBox" src={ box } />

                </span>
                <span id="centralVideo" className={ id }>
                    <ReactPlayer width="98%" height="95%" id={ id } url="http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4"
                        playing={ play }
                        onPause={ () => setPlay(false) }
                        onSeek={ () => setPlay(true) }
                        controls={ play }
                    />
                    { !play && <FaPlay onClick={ () => setPlay(true) } /> }
                </span>
                <span className="secondVideo">
                    <video src="http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4" />
                    <p>One man show</p>
                    <img className="secondVideoBox" src={ box } />
                </span>
                <span>
                    <video src="http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4" />
                    <p>What a shot</p>
                    <img className="videoBox" src={ box } />
                </span>
            </div>
            <div id="underTitle">
                <h2>Goal of the season all ready?</h2>
            </div>
            <section>
                <p>See the <a href="/">Featured Highlights</a> page for a full collection of our best clips from across all sports.
            </p>
                <p>
                    Higlight of the Week will feature on the app and website, the Sport Stream social media channels and saved in the archives to look back on.
            </p>
            </section>
        </div>


    );
}

export default HomeVideos;