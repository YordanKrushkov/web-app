import './index.scss';
// import { Link } from 'react-router-dom'
import WebHeader from './WebHeader'
import { useEffect, useState } from 'react'
import MobileHeader from '../MobileHeader'
import { useContext } from 'react'
import { AuthContext } from '../../../Context/auth';
const Header = ({ title, secondTitle, subTitle }) => {
    const { isAuth, logout } = useContext(AuthContext)
    const [width, setWidth] = useState(window.innerWidth < 1025)
    let header =
        useEffect(() => {
        }, [width])
    return (
        <div className="headersWrapper">
            {
                width
                    ? <MobileHeader
                        title={ title }
                        secondTitle={ secondTitle }
                        subTitle={ subTitle }
                        isAuth={ isAuth }
                        logout={ logout }
                    />
                    : <WebHeader
                        title={ title }
                        secondTitle={ secondTitle }
                        subTitle={ subTitle }
                        isAuth={ isAuth }
                        logout={ logout }
                    />
            }
        </div>

    );
}

export default Header;