import './index.scss';
const AdminHeader = ({logout}) => {
    return (
        <div id="adminHeader">
            <ul>
                <li>Requests</li>
                <li>Messages</li>
                <li onClick={logout}>Sign Out</li>
            </ul>
        </div>
    );
}

export default AdminHeader;