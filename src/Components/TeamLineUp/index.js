import './index.scss';
import Footer from '../Footer'
const TeamLineUp = () => {
    return (
        <div style={{paddingTop:"60px"}}>
        <div id='teamLineUpWrapper'>
            <div>
                <h1>Team line-ups</h1>
                <div>
                    <section>
                        <h3>Reigate priory</h3>
                        <p>Richie Oliver</p>
                        <p>Sam Hail</p>
                        <p>Bradley Scriven</p>
                        <p>Andy Delmont</p>
                        <p>Luke Haughton</p>
                        <p>Angus Dahl</p>
                        <p>Luke Beaven</p>
                        <p>Richard Stevens</p>
                    </section>
                    <section>
                        <h3>Reigate priory</h3>
                        <p>Richie Oliver</p>
                        <p>Sam Hail</p>
                        <p>Bradley Scriven</p>
                        <p>Andy Delmont</p>
                        <p>Luke Haughton</p>
                        <p>Angus Dahl</p>
                        <p>Luke Beaven</p>
                        <p>Richard Stevens</p>
                    </section>
                </div>
            </div>
            <div id="scoreCard">
                <div>
                    <h3>Integrated Scorecard/score sheet etc</h3>
                </div>
            </div>
        </div>
            <Footer/>
        </div>
    );
}

export default TeamLineUp;