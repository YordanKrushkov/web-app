import { useState, useEffect } from 'react';
import './index.scss';
import { FaPlay } from "react-icons/fa";
import ReactPlayer from 'react-player';
import box from '../../../Assets/images/white-box.png';


import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";

const HomeVideoMobile = ({ videos, setVideos }) => {
    const [play, setPlay] = useState(false);
    const [mainVideo, setMain] = useState(videos[2]);
    const [id, setId] = useState('pauseVideo');

    useEffect(() => {
        if (play) {
            setId("playVideo")
        } else {
            setId('pauseVideo')
        }
        console.log(id);
    }, [play, id]);

    const clickHandler = (e) => {
        if (e !== 2) {
            let video, newArr, el
            switch (e) {
                case e = 0:
                    setMain(videos[e])
                    video = videos.splice(e, 3);
                    newArr = videos.concat(video)
                    setVideos(newArr)
                    break
                case e = 1:
                    setMain(videos[e])
                    newArr = videos;
                    el = newArr.pop()
                    newArr.unshift(el)
                    setVideos(newArr)
                    break;
                case e = 3:
                    setMain(videos[e])
                    newArr = videos;
                    el = newArr.shift()
                    newArr.push(el)
                    setVideos(newArr)
                    break;
                case e = 4:
                    setMain(videos[e])
                    video = videos.splice(2, e);
                    newArr = video.concat(videos)
                    setVideos(newArr)
                    break;
            }
        }
    }

    const changeVideos = (e) => {
        const index = videos.indexOf(mainVideo)
        if (e.currentTarget.id === 'arrowForward') {
            if (index + 1 === videos.length) {
                setMain(videos[0]);
            } else {
                setMain(videos[index + 1]);
            }
        } else if (e.currentTarget.id === 'arrowBack') {
            if (index - 1 < 0) {
                setMain(videos[videos.length - 1]);
            } else {
                setMain(videos[index - 1]);
            }
        }
    }


    return (
        <div id="videoHomeMobileWrapper">
            <IoIosArrowBack id="arrowBack" onClick={ changeVideos } />
            <span id="centralVideo" className={ id }>
                <ReactPlayer width="95%" height="95%" id={ id } url={ mainVideo.url }
                    playing={ play }
                    onPause={ () => setPlay(false) }
                    onSeek={ () => setPlay(true) }
                    controls={ play }
                    light={mainVideo.poster}
                    className="mainMobileVideos"
                />
                { !play && <FaPlay onClick={ () => setPlay(true) } /> }
                <h2>{ mainVideo.title }</h2>
            </span>
            <IoIosArrowForward id="arrowForward" onClick={ changeVideos } />
        </div>
    );
}

export default HomeVideoMobile;