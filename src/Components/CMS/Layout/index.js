import AdminHeader from '../Header'
import {useContext} from 'react';
import {AdminAuthContext} from '../../../Context/adminsAuht'
import Messages from "../Messages";
const AdminLayout = () => {
const {logout} = useContext(AdminAuthContext)

    return ( 
        <div>
        <AdminHeader logout={logout}/>
        <Messages />
        </div>
     );
}
 
export default AdminLayout;