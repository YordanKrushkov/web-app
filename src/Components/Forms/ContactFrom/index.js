import './index.scss'
import { useState, useContext } from 'react';
import counties from '../../../Data/counties'
// import {register} from '../../../Services/authServices'
import auth from '../../../Services/auth'
import { AuthContext } from '../../../Context/auth'
const ContactForm = () => {
    const [placeHolder, setPlaceHolder] = useState({
        sport: true,
        package: true,
        county: true
    });
    const [contact, setContact] = useState({
        Club: '',
        FirstName: '',
        LastName: '',
        Email: '',
        Phone: '',
        Affiliation: '',
        County: '',
        Sport: '',
        Package: ''
    })
    const { login } = useContext(AuthContext);
    const changeHandler = (e) => {
        if (e.target.value !== '') {
            setPlaceHolder({
                ...placeHolder,
                [e.target.name]: false
            })
        }
    }
    const submitHandler = (e) => {
        e.preventDefault();
        setContact({
            club: e.target.club.value,
            firstName: e.target.firstName.value,
            surname: e.target.surname.value,
            email: e.target.email.value,
            phone: e.target.phone.value,
            affiliation: e.target.affiliation.value,
            county: e.target.county.value,
            sport: e.target.sport.value,
            package: e.target.package.value
        });

        auth('/contact',
            {
                club: e.target.club.value,
                firstName: e.target.firstName.value,
                surname: e.target.surname.value,
                email: e.target.email.value,
                phone: e.target.phone.value,
                affiliation: e.target.affiliation.value,
                county: e.target.county.value,
                sport: e.target.sport.value,
                package: e.target.package.value
            },
            contact, login)
    }
    return (
        <form className="contactForm" onSubmit={ submitHandler }>
            <input type="text" name="club" placeholder="Name of club/team" />
            <h4>Name of Contact</h4>
            <div>
                <input type="text" name="firstName" placeholder="First Name" />
                <input type="text" name="surname" placeholder="Surname" />
            </div>
            <input type="text" name="email" placeholder="Email Address" />
            <div>
                <input type="text" name="phone" placeholder="Contact Number" />
            </div>
            <input type="text" name="affiliation" placeholder="Affiliation with Club: Eg, playing member, chairman, director" />
            <h4>
                Club Details
        </h4>
            <select name='county' style={ placeHolder.county ? { color: "#999" } : null } onChange={ changeHandler }>
                <option value="" disabled={ !placeHolder.county }>County</option>
                { counties && counties.map(x => <option key={ x.abbreviation } value={ x.name }>{ x.name }</option>) }
            </select>
            <select name='sport' style={ placeHolder.sport ? { color: "#999" } : null } onChange={ changeHandler }>
                <option value="" disabled={ !placeHolder.sport }>Sport</option>
                <option value="cricket">Cricket</option>
                <option value="rugby">Rugby</option>
                <option value="football">Football</option>
            </select>
            <select name='package' style={ placeHolder.package ? { color: "#999" } : null } onChange={ changeHandler }>
                <option id="greenText" value="" disabled={ !placeHolder.package }>Package</option>
                <option value="gold">Gold</option>
                <option value="silver">Silver</option>
                <option value="bronze">Bronze</option>
            </select>
            <button>Confirm</button>
        </form>
    );
}

export default ContactForm;