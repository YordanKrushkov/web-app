import { useState } from 'react';
import {Link} from 'react-router-dom'
import './index.scss';
import HomeVideoRow from './HomeVideoRow';
import HomeVideoMobile from './HomeVideoMobile';
import Hockey from '../../Assets/images/HOCKEY.png'
import Football from '../../Assets/images/FOOTBALL.png'
import CRICKET from '../../Assets/images/CRICKET.png'
import RUGBY from '../../Assets/images/RUGBY.png'
import TENNIS from '../../Assets/images/TENNIS.png'
const HomeVideos = ({width}) => {
    const [videos, setVideos] = useState([
        {
            title: "Sensational try",
            url: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
            poster:Hockey
        },
        {
            title: "Did she catch that?",
            url: "http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4",
            poster:Football

        },
        {
            title: "Goal of the season all ready?",
            url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4",
            poster:CRICKET

        },
        {
            title: "One man show",
            url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4",
            poster:RUGBY

        },
        {
            title: "What a shot",
            url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4",
            poster:TENNIS

        },
    ]);




    return (
        <div id="homeVideos">
            <h1>Sport stream in action</h1>
            { width
                ? <HomeVideoRow videos={ videos } setVideos={ setVideos } />
                : <HomeVideoMobile videos={ videos } setVideos={ setVideos } />
            }

            <div id="underTitle">
            </div>
            <section>
                <p>See the <Link to="/highlights">Featured Highlights</Link> page for a full collection of our best <br id="break"/> clips from across all sports.
                </p>
                <p>
                    Highlight of the Week will feature on the app and website, the Sport Stream social media channels and saved in the archives to look back on.
                </p>
            </section>
        </div>


    );
}

export default HomeVideos;