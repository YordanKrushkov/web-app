const baseUrl = 'http://localhost:4000'

const register = async(body) => {
const reg= await fetch(`${baseUrl}/auth/register`, {
        method: "POST",
        'headers': {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    })
const res=await reg;
const data= await res.headers.get('Authorization')
return data
}

export {
    register
}