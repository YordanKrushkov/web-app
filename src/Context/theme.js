import React, { Component, createContext } from 'react';
export const ThemeContext = createContext()
class ThemeContextProvider extends Component {
    state = {
        stripeColor: '#fff',
        previewColor: '#fff',
        original: "#fff"
    }

    changeStripe = (color) => {
        this.setState({
            stripeColor: color
        })
    }
    setPreviewColor = (color) => {
        console.log(color);
        this.setState({
            previewColor: color
        })
    }
    resetStripe = () => {
        this.setState({
            stripeColor: '#fff',
            previewColor: '#fff'
        })
    }
    render() {
        return (
            <ThemeContext.Provider value={ { ...this.state, changeStripe: this.changeStripe, resetStripe: this.resetStripe, setPreviewColor: this.setPreviewColor } }>
                {this.props.children }
            </ThemeContext.Provider>
        )
    }
};

export default ThemeContextProvider;