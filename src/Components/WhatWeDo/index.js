import SVG from '../SVG';
import Footer from '../Footer'
import camera from '../../Assets/images/Camera.png'
import laptop from '../../Assets/images/Laptop-and-Phone.png'
import stopWatch from '../../Assets/images/Stopwatch.png';
import './index.scss';
import { useState } from 'react';
const WhatWeDo = () => {
    const [width, setWid] = useState(window.innerWidth > 1025)
    return (
        <div id="whatWeDo">
            <header>
                <h1>What we do</h1>
                <p>Sport stream is a recording and streaming service that captures and broadcasts live sport for amateur and semi professional clubs. With the flexability of multiple cameras, graphic overlays, highlights and live updates. Sport Stream brings a level-up to amateur sports broadcasting </p>
            </header>
            <main>
                <div>
                    <img src={ camera } alt="" />
                    <p>We provide cameras and easy-to-use, high quality streaming equipment, ready to broadcast your games live and from multiple angles</p>
                </div>
                <div id="laptopImageWrapper">
                    <img src={ laptop } id="laptopImage" alt="" />
                    <p>The game is streamed live to our app and website, as well as other platforms on request, such as Youtube and Facebook</p>
                </div>
                <div>
                    <img src={ stopWatch } alt="" />
                    <p>After the game, catch up with post-match highlights, both general and play specific, so you can watch all the best moments as you wish</p>
                </div>
            </main>
            <Footer />
        </div>
    );
}

export default WhatWeDo;