import './index.scss';
import { Link } from 'react-router-dom';
import { useEffect } from 'react'
import logo from '../../Assets/images/logo-white.png'
import facebook from '../../Assets/images/Facebook Logo.png'
import instagram from '../../Assets/images/Instagram Logo.png'
const Footer = () => {


    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])
    return (
        <div id="footerWrapper">
            <nav>
                <Link to="/">Home</Link>
                <Link to="/about">About</Link>
                <Link to="/highlights">Featured Highlights</Link>
                <Link to="/packages">Packages</Link>
                <Link to="/myclub">My club</Link>
                <Link to="/contact">Contact</Link>
            </nav>
            <img id="footerLogo" src={ logo } alt="" />
            <div>
                <section>
                    <a href="">
                        <img className="fb" src={ facebook } alt="" />
                    </a>
                    <a href="">
                        <img className="fb" src={ instagram } alt="" />
                    </a>
                </section>
                <p>Follow us for more content</p>
            </div>
        </div>
    );
}

export default Footer;