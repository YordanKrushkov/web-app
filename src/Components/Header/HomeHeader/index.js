import './index.scss';
import { useState, useContext } from 'react';
import { AuthContext } from '../../../Context/auth';
import MobileHeader from '../MobileHeader';
import WebHomeHeader from './WebHomeHeader'
const HomeHeader = () => {
    const [width, setWidth] = useState(window.innerWidth < 1025)
    const { isAuth, logout } = useContext(AuthContext);


    return (
        <div className="headersWrapper">
            {
                width
                    ? <MobileHeader />
                    : <WebHomeHeader isAuth={ isAuth } logout={ logout } />
            }
        </div>

    );
}

export default HomeHeader;