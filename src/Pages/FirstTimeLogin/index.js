import './index.scss';
import { useState } from 'react';
import LoginFirstForm from '../../Components/Forms/LoginFirstForm';
import LoginFirstSettings from '../../Components/Forms/LogingFirstSettings';
import logo from '../../Assets/images/logo-white.png'
const FirstTimeLogin = () => {
  const [next, setNext] = useState(false)
  return (
    <div id="firstTimeLoginPage">
      <img src={ logo } alt="website logo" />
      <h4>
        We see it's your first time logging in from an admin account, so there's a few things to set-up before we create your club's page...
        </h4>
      {next
        ? <LoginFirstSettings />
        : <LoginFirstForm setNext={ setNext } />
      }
    </div>
  );
}

export default FirstTimeLogin;