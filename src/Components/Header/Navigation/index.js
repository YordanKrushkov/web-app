import { Link } from 'react-router-dom'
import './index.scss';
const Nav = ({ isAuth, logout, home }) => {

    let link= isAuth ? <Link to="/" onClick={ logout }>Sign out</Link> : <Link to="/login">Log in</Link>
    return (
        <nav className={isAuth ? 'nav authNav' : 'nav'}>
            <Link to="/">Home</Link>
            <Link to="/about">About</Link>
            <Link to="/highlights">Featured Highlights</Link>
            <Link to="/packages">Packages</Link>
            {isAuth && <>
                <Link to="/myclub">My club</Link>
                <Link to="/admin">Admin</Link>
            </>
            }

            <Link to="/contact">Contact</Link>
            {!home && link
            }
        </nav>
    );
}

export default Nav;