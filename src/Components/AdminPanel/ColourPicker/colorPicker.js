import { ColorPicker, useColor } from "react-color-palette";
import "react-color-palette/lib/css/styles.css";
import './index.scss'
import {ThemeContext} from '../../../Context/theme';
import {useContext,useEffect} from 'react';

const Colors = () => {
    const [color, setColor] = useColor("hex", "");
    const {setPreviewColor}=useContext(ThemeContext)
    useEffect(()=>{
        if(color.hex!=="#000000"){
            setPreviewColor(color.hex)
        }
    },[color])
    return <ColorPicker id="colorPicker" width={ 456 } 
    height={ 228 } 
    color={ color } 
    onChange={ setColor} 
    dark   
    />;
};

export default Colors