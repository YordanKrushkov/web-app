import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import reportWebVitals from './reportWebVitals';
import ThemeContextProvider from './Context/theme'
import AuthContextProvider from './Context/auth'
import AdminAuthContextProvider from './Context/adminsAuht'

ReactDOM.render(
  <React.StrictMode>
    <ThemeContextProvider>
      <AdminAuthContextProvider>
        <AuthContextProvider>
          <App />
        </AuthContextProvider>
      </AdminAuthContextProvider>
    </ThemeContextProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
