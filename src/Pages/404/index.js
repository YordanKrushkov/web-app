import './index.scss';
import logo from '../../Assets/images/logo-white.png';
import {Link} from 'react-router-dom'
const Page404 =()=>{

    return (
        <div id="Wraper_404">
            <div>
                <h1>Looks like you are lost...</h1>
                <p>Were you looking for us? Navigate to our <Link to='/'>home page</Link></p>
            </div>
            <div id="Logo_404">
                <img src={logo} alt="logo" />
            </div>
        </div>
    )
}

export default Page404