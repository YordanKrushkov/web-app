import './index.scss';
import Header from '../../Components/Header/Header';
import logo from '../../Assets/images/Reigate-Priory-Logo.png'
import MoreVideos from '../../Components/MoreVideos'
import { FaPlay } from "react-icons/fa";
import ReactPlayer from 'react-player'
import { useState, useEffect } from 'react'
import TeamLineUp from '../../Components/TeamLineUp'
const MyClub = () => {
    const [play, setPlay] = useState(false);
    const [id, setId] = useState('pouseMyClubVideo');

    useEffect(() => {
        if (play) {
            setId("playMyClubVideo")
        } else {
            setId('pouseMyClubVideo')
        }
        console.log(id);
    }, [play])
    return (
        <div>
            <div id="myClubWrapper">
                <Header title="My Club" subTitle="Reigate priory cc" />

                <main>
                    <div>
                        <img src={ logo } alt="" />
                        {/* <div className="clubInfo">
                    <p>The home of your clubs' Sport Stream footage</p>
                    <p>Cathc up on all of your club's fixtures with full match replays, general and specific higlights and of course watch the games live right here, or on the move, via the Sport Stream app.</p>
                    </div> */}

                    </div>
                    <div id="myClubVideo" className={ id }>
                        <h1>Reigate Priory vs banstead 28.08.21</h1>
                        <div>
                            <ReactPlayer width="98%" height="95%" url="http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4"
                                playing={ play }
                                onPause={ () => setPlay(false) }
                                onSeek={ () => setPlay(true) }
                                controls={ play }
                                style={ { border: "none" } }
                            />
                            { !play && <FaPlay onClick={ () => setPlay(true) } /> }
                        </div>
                        <h5>Full match replay</h5>
                    </div>
                    <div id="myClubIfno">
                        <p>League Fixture from the Surrey Championship Premier Devision</p>
                        <p>Venue: Park Lane Road</p>
                        <p>Reigate Priory won the toss and elected to bat</p>
                        <p>Match info and scorecards below</p>
                    </div>
                </main>
                <MoreVideos text={ "More videos from this fixture:" } />
            </div>
            <div style={ { position: "relative" } }>
                <TeamLineUp />
            </div>
        </div>
    );
}

export default MyClub;