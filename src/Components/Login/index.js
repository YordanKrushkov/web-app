import './index.scss';
import logo from '../../Assets/images/logo-white.png'
import Header from '../../Components/Header/Header';
import HomeHeader from '../../Components/Header/HomeHeader'
import {Link} from 'react-router-dom'

const LoginForm = ({submitHandler,text,regHandler}) => {
    return (
        <div>
        {!text && <HomeHeader />}
            <div id="loginPage">
                <img src={ logo } alt="website logo" />
                {text && <h3>{text}</h3>}
                <form onSubmit={submitHandler}>
                    <input type="text" name="email" placeholder="Email Address" />
                    <input type="password" name="password" placeholder="Password" />
                   <div>
                   <p>Don't have an account?<b onClick={regHandler}>Sign up</b></p>
                    <button>Login</button>
                   </div>
                </form>
            </div>
        </div>

    );
}

export default LoginForm;