import { useContext } from 'react';
import { ThemeContext } from '../../../Context/theme'
const WebSVG = () => {
  const { stripeColor, previewColor,original } = useContext(ThemeContext);
  let color = previewColor !== stripeColor ? previewColor : stripeColor;
  let opacity=previewColor !== original ? 0.4 : 0.03;
  return (
    <svg id="strype" width="100%" height="100%" viewBox="0 0 1440 800" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M-513.173 997.045L1816.55 -380.956L1769.59 349.311L-553.173 997.045Z"
        fill={ color } fillOpacity={ opacity } />
    </svg>
  );
}
export default WebSVG;