import './index.scss';
import { useState, useEffect } from 'react';
import WebSVG from './Web';
import SVGMobile from './Mobile';
const SVG = () => {
  const [width, setWidht] = useState(window.innerWidth);
  console.log(width);
  useEffect(() => {
  }, [width])
  return (
    width && width > 800
      ? <WebSVG/>
      : <SVGMobile />
  );
}
export default SVG;


