import './index.scss';
import Nav from '../Navigation';


const WebHeader = ({ title, secondTitle, subTitle, isAuth, logout }) => {
    return (
        <div id="headerWrapper">
            <div>
                <h1>{ title }</h1>
                { secondTitle && <h1>{ secondTitle }</h1> }
                { subTitle && <h2>{ subTitle }</h2> }
            </div>
            <Nav isAuth={ isAuth } logout={ logout } />
        </div>
    );
}

export default WebHeader;
