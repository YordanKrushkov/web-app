import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import './App.scss';
import Home from './Pages/Home'
import MyClubPage from './Pages/MyClubPage'
import MyClub from './Pages/MyClub'
import About from './Pages/About';
import Hightlights from './Pages/HightLights'
import Packages from './Pages/Packages';
import FirstTimeLogin from './Pages/FirstTimeLogin';
import Admin from './Pages/Admin';
import Login from './Pages/Login';
import Contact from './Pages/Contact';
import CMS from './Pages/CMS';
import VideoControl from './Pages/VideoControl';
import SVG from './Components/SVG';
import Page404 from './Pages/404';

function App() {
  return (
    <div className="App">
      <SVG />
      <Router basename="/web-app/">
        <Switch>
          <Route exact path="/" component={ Home } />
          <Route exact path="/first-login" component={ FirstTimeLogin } />
          <Route exact path="/login" component={ Login } />
          <Route path="/myclub" component={ MyClubPage } />
          <Route path="/highlights" component={ Hightlights } />
          <Route path="/myclub:id" component={ MyClub } />
          <Route path="/about" component={ About } />
          <Route path="/packages" component={ Packages } />
          <Route path="/admin" component={ Admin } />
          <Route path="/contact" component={ Contact } />
          <Route path="/cms" component={ CMS } />
          <Route path="/control" component={ VideoControl } />
          <Route path="/" component={ Page404 } />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
