import  baseUrl  from './endpoints'
const auth = async (url, body, onSucces,cookie) => {
    try {
        const promise = await fetch(`${baseUrl}auth${url}`, {
            method: "POST",
            body: JSON.stringify(body),
            'headers': {
                'Content-Type': 'application/json'
            },
        })
        const authToken = promise.headers.get('Authorization');
        document.cookie = `${cookie}-auth-token=${authToken}`;
        const response = await promise.json();
        if (response.email && authToken && response._id) {
            onSucces(response);
            localStorage.setItem("user", response.email);
        } else {
            // onFail()
        }
    } catch (error) {
        // onFail()
        console.log("auth:", error);
    }

}

export default auth;