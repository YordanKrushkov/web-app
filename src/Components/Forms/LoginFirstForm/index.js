import './index.scss';

const LoginFirstForm = ({setNext}) => {
    return (
        <div id="loginFirstPage">
            <h1>Reigate priory cc</h1>
            <form>
                <h4>Lead Admin for your club</h4>
                <div>
                    <input type="text" placeholder="First Name" />
                    <input type="text" placeholder="Surname" />
                </div>
                <input type="text" placeholder="Email Address" />
                <div>
                    <input type="text" placeholder="Contact Number" />
                </div>
                <button onClick={()=>setNext(true)}>Next</button>
            </form>
        </div>

    );
}

export default LoginFirstForm;