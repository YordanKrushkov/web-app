import { useState,useEffect } from 'react';
import './index.scss';
import { FaPlay } from "react-icons/fa";
import ReactPlayer from 'react-player';
import box from '../../../Assets/images/white-box.png';


const HomeVideoRow = ({ videos,setVideos}) => {
    const [play, setPlay] = useState(false);
    const [mainVideo, setMain] = useState(videos[2]);
    const [id, setId] = useState('pauseVideo');

    useEffect(() => {
        if (play) {
            setId("playVideo")
        } else {
            setId('pauseVideo')
        }
        console.log(id);
    }, [play, id]);

    const clickHandler = (e) => {
        if (e !== 2) {
            let video, newArr, el
            switch (e) {
                case e = 0:
                    setMain(videos[e])
                    video = videos.splice(e, 3);
                    newArr = videos.concat(video)
                    setVideos(newArr)
                    break
                case e = 1:
                    setMain(videos[e])
                    newArr = videos;
                    el = newArr.pop()
                    newArr.unshift(el)
                    setVideos(newArr)
                    break;
                case e = 3:
                    setMain(videos[e])
                    newArr = videos;
                    el = newArr.shift()
                    newArr.push(el)
                    setVideos(newArr)
                    break;
                case e = 4:
                    setMain(videos[e])
                    video = videos.splice(2, e);
                    newArr = video.concat(videos)
                    setVideos(newArr)
                    break;
            }
        }
    }

    return (
        <div id="videoHomeRowWrapper">
            { videos.map((x, index) => (
                index !== 2
                    ? <span className={ index === 1 || index === 3 ? 'secondVideo' : '' } key={ index } onClick={ () => clickHandler(index) }>
                        <video src={ x.url } poster={x.poster}/>
                        <img className={ index === 1 || index === 3 ? 'secondVideoBox' : 'videoBox' } src={ box } />
                        <p>{ x.title }</p>
                    </span>
                    : <span id="centralVideo" className={ id } key={ index }>
                        <ReactPlayer width="98%" height="95%" id={ id } url={ mainVideo.url }
                            playing={ play }
                            onPause={ () => setPlay(false) }
                            onSeek={ () => setPlay(true) }
                            controls={ play }
                            light={mainVideo.poster}
                            
                        />
                        { !play && <FaPlay onClick={ () => setPlay(true) } /> }
                        <h2>{ mainVideo.title }</h2>
                    </span>
            )) }
        </div>
    );
}

export default HomeVideoRow;