import { Component, createContext } from 'react';
import { userVerify } from '../Services/verify'
export const AuthContext = createContext()

class AuthContextProvider extends Component {
    state = {
        isAuth: false,
        firstName: '',
        surname: '',
        email: '',
    }

    login = (data) => {
        this.setState({
            isAuth: true,
            firstName: data.firstName,
            surname: data.surname,
            email: data.email
        })
    }
    logout = () => {
        document.cookie = "x-auth-token=";
        this.setState({
            isAuth: false,
            firstName: '',
            surname: '',
            email: ''
        })
    }

    componentDidMount() {
        userVerify().then((res) => {
            if (res) {
                this.setState({
                    userID: res._id,
                    isAuth: true,
                    email: res.email,
                    firstName: res.firstName,
                    surname: res.surname,
                })
            } else {
                this.logout();
            }
        })
            .catch((err) => console.log(err));
    }

    render() {
        return (
            <AuthContext.Provider value={ { ...this.state, login: this.login, logout: this.logout } }>
                {this.props.children }
            </AuthContext.Provider>
        )
    }

}

export default AuthContextProvider;