import './index.scss';
import {useRef} from 'react'
import colourWheel from '../../../Assets/images/Colour-Wheel.png'
const LoginFirstSettings = () => {
    const myRef = useRef(null)
    const handleClick = () => {
        myRef.current.click();
     }
    return ( 
        <div id="loginFirstSettings">
            <h1>Reigate priory cc</h1>
            <form>
                <div>
                    <input type="text" placeholder="Upload Club Badge/Icon" />
                    <input id="uploadImage" ref={myRef} style={{display:'none'}} type="file" />
                    <input id="uploadImageButton" value="Upload File" type="button" onClick={handleClick}/>
                    <p>Please select an image with a transparent background if possible</p>
                </div>
                <div>
                    <section>
                    <input id="pickColor" type="text" placeholder="Select Primary Colour" />
                    <img src={colourWheel}  alt="colour" />
                    </section>
                    <p>This colour make up the custom background for users with access to your club's footage</p>
                </div>
                <div>
                    <input id="pickColor" type="text" placeholder="3-Letter Abbreviation" />
                    <p>On score cards and when shortened versions of the name are required, we will use a 3-letter abbreviation name</p>
                </div>
                <button>Confirm</button>
            </form>
        </div>
     );
}
 
export default LoginFirstSettings;