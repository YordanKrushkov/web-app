import './index.scss'
import box from '../../Assets/images/white-box.png'
import { Link } from 'react-router-dom'
const MoreVideos = ({ text, secondText }) => {
    return (
        <div id="moreVideosWrapper">
            {secondText
                ? <div id="textOver">
                    <h1>{ text }</h1>
                    <p>{ secondText }</p>
                </div>
                : <h1 className="oneTitle">{ text }</h1>
            }
            {/* <div id="textOver">
                <h1>{ text }</h1>
                { secondText && <p>{ secondText }</p> }
            </div> */}
            <div>
                <Link to="/myclub:id">
                    <video src="http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4" />
                    <img className="videoBox" src={ box } />
                    <p>Sensational try</p>
                </Link>
                <Link to="/myclub:id">
                    <video src="http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4" />
                    <img className="videoBox" src={ box } />
                    <p>Sensational try</p>
                </Link>
                <Link to="/myclub:id">
                    <video src="http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4" />
                    <img className="videoBox" src={ box } />
                    <p>Sensational try</p>
                </Link>
                <Link to="/myclub:id">
                    <video src="http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4" />
                    <img className="videoBox" src={ box } />
                    <p>Sensational try</p>
                </Link>
            </div>
        </div>
    );
}

export default MoreVideos;