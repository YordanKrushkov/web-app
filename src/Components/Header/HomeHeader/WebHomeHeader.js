import './index.scss';
import { Link } from 'react-router-dom';
import Nav from '../Navigation'

const WebHomeHeader = ({ isAuth, logout }) => {
    return (
        <div id="homeHeaderWrapper">
            <Nav isAuth={ isAuth } home={ true } />
            {isAuth
                ? <Link to="/" onClick={ logout }>Sign out</Link>
                : <Link to="login">Log in</Link>
            }
        </div>
    );
}

export default WebHomeHeader;