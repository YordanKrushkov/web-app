import './index.scss';
import Header from '../../Components/Header/Header';
import PackageCard from '../../Components/PackageCard';
import packages from '../../Data/Packages';
import Footer from '../../Components/Footer';
import ContactForm from '../../Components/Forms/ContactFrom';
const Packages = () => {
    return (
        <div id="packages">
            <section style={ { height: "98vh" } }>
                <Header title="Packages" />
                <main>
                    {
                        packages && packages.map(x =>
                            <PackageCard key={x.package} packages={ x } />
                        ) }
                </main>
            </section>
            <section id="sectionEnquire">
                <div id="enquire">
                    <header>
                        <h3>Fill in the details bellow and a member of our team will be in touch to discuss and finalise your plan further. If you just want to enquire or ask us a question, then email us at jordan@sport-stream.co.uk</h3>
                    </header>
                    <ContactForm />
                </div>
                <Footer />
            </section>
        </div>
    );
}

export default Packages