
import './index.scss'
import img from '../../Assets/images/homepage.png';
import logo from '../../Assets/images/logo-white.png';

import SVG from '../../Components/SVG'
import WhatWeDo from '../../Components/WhatWeDo'
import HomeVideos from '../../Components/HomeVideos'
import HomeHeader from '../../Components/Header/HomeHeader'
import { useState } from 'react';
const Home = () => {
    const [width, setWidht] = useState(window.innerWidth > 1025)
    return (
        <div>
            <HomeHeader />
            <div id="homeWrapper" style={ { backgroundImage: `url(${img})` } }>
                <div id="homePageLogo">
                    <img src={ logo } alt="website logo" />
                    <p>Imagine the greatest game you've ever played in... and now you could watch it all back again</p>
                </div>
            </div>
            { !width
                ? <div style={ { height: "100vh", position: "relative" } }>
                    <HomeVideos />
                    <WhatWeDo />
                </div>
                : <>
                    <HomeVideos width={width} />
                    <WhatWeDo width={width} />
                </>
            }

        </div>
    );
}

export default Home;