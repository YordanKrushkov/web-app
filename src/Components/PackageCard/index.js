import './index.scss';
import medal from '../../Assets/images/Gold-Medal.png'
const PackageCard = ({ packages }) => {
    let i = 0;
    return (
        <div id="packageCard">
            <div>
                <header>
                    <img src={ packages.medal } alt="medal" />
                    <h1>{ packages.package }</h1>
                </header>
                <section>
                    { packages.p.map(x => {
                        i++;
                        return (<p key={ i}>{ x }</p>)
                    }) }
                </section>
            </div>
        </div>
    );
}

export default PackageCard