import './index.scss';
import Header from '../../Components/Header/Header';
import add from '../../Assets/images/White-Plus.png'
import pen from '../../Assets/images/Pen.png'
import badge from '../../Assets/images/Badge-Icon.png'
import wheel from '../../Assets/images/Colour-Wheel.png'
import Footer from '../../Components/Footer'
import { useState } from 'react'
import ColorPicker from '../../Components/AdminPanel/ColourPicker'
import AddAdmins from '../../Components/AdminPanel/addAdmins'
const Admin = () => {
    const [show, setShow] = useState({
        addFixture: true,
    });

    const changeHandler = (e) => {
        if (show[e.currentTarget.id] !== e.currentTarget.id) {
            setShow({
                [e.currentTarget.id]: true
            })
        }
    }
    const colorSet = () => {
        setShow({
            addFixture: true  
        })
    }
    return (
        <div id="admin">
            <div>
                <Header title="Admin" />
                <main>
                    <p>
                        Update your club's fixtures, team sheets and match details here. You can also edit the layout of the pages by adjusting the colour scheme or updating the club badge/icon.
                    </p>
                    <nav>
                        <div id="addFixture" onClick={ changeHandler }>
                            <img src={ add } alt="add" />
                            <h2>add/edit fixtures</h2>
                        </div>
                        <div id="editTeam" onClick={ changeHandler }>
                            <img src={ pen } alt="add" />
                            <h2>edit team line-ups</h2>
                        </div>
                        <div id="adjustColor" onClick={ changeHandler }>
                            <img src={ wheel } alt="add" />
                            <h2>adjust colour scheme</h2>
                        </div>
                        <div id="updateBadge" onClick={ changeHandler }>
                            <img src={ badge } alt="add" />
                            <h2>update badge/icon</h2>
                        </div>
                    </nav>
                    <div className="whiteLine"></div>
                    { show.addFixture && <AddAdmins /> }
                    { show.adjustColor && <ColorPicker colorSet={colorSet}/> }

                </main>
            </div>
            {/* <div style={ { marginTop: '5%', position: "relative", } }>
                <Footer />
            </div> */}
        </div>
    );
}

export default Admin;