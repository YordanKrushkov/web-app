import './index.scss';
import { HiOutlineMenu } from "react-icons/hi";
import { Link } from 'react-router-dom';
import { useState } from 'react';
const MobileHeader = ({ title, secondTitle, subTitle, isAuth, logout }) => {
    const [show, setShow] = useState(false);
    const clickHandler = (e) => {
        setShow(false)
    }
    return (
        <div id="mobileNav">
               <div id="mobileHeaderTitle">
                <h1>{ title }</h1>
                { secondTitle && <h1>{ secondTitle }</h1> }
                { subTitle && <h2>{ subTitle }</h2> }
            </div>
            <HiOutlineMenu onClick={ () => setShow(!show) } />
            {show &&
                <nav onClick={ clickHandler }>
                
                    <Link to="/">Home</Link>
                    <Link to="/about">About</Link>
                    <Link to="/highlights">Featured Highlights</Link>
                    <Link to="/packages">Packages</Link>
                    {isAuth 
                    &&
                    <>
                    <Link to="/myclub">My club</Link>
                    <Link to="/admin">Admin</Link>
                    </>
                    }
                    <Link to="/contact">Contact</Link>
                    {isAuth
                    ?<Link to="/">Sign out</Link>
                    :<Link to="/login">Log in</Link>
                    }
                </nav>
            }
        </div>
    );
}

export default MobileHeader;