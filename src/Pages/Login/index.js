import { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import auth from '../../Services/auth';
import { AuthContext } from '../../Context/auth';
import LoginForm from '../../Components/Login';
import Registration from '../../Components/Forms/Registration';

const Login = () => {

  const { login } = useContext(AuthContext);
  const [isRegistered, setRegistered] = useState(false)
  const history = useHistory()

  const regHandler = () => {
    setRegistered(true)
  }

  const submitHandler = (e) => {
    e.preventDefault();

    auth('/login', {
      email: e.target.email.value,
      password: e.target.password.value
    }, login, 'x')
      .then(() => history.push('/'))
  }

  return (
    !isRegistered
      ? <LoginForm
        submitHandler={ submitHandler }
        regHandler={ regHandler }
      />
      : <Registration />
  );
}

export default Login;