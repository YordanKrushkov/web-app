import { MdDelete } from "react-icons/md";
import './index.scss';
import { useState } from 'react';
const AddAdmins = () => {
    const [show, setShow] = useState(false)
    const [admin, setAdmin] = useState([
        {
            name: 'John Smith',
            email: 'Johnsmith@gmail.com'
        },
        {
            name: 'Joe Bloggs',
            email: 'Joebloggs@gmail.com'
        },
        {
            name: 'Chris Stevens',
            email: 'Crhisstevens@gmail.com'
        },

    ])

    const submitHandler = (e) => {
        e.preventDefault();
        let newAdmin={
            name: e.target.name.value,
                email: e.target.email.value
        }
        if(newAdmin.name&&newAdmin.email){
            setAdmin(admin=>[...admin,newAdmin])
            setShow(false)
        }
}

const deleteHandler=(e)=>{
    let del=e.currentTarget.parentElement.firstChild.innerHTML;
    let filtred=admin.filter(x=>x.name !== del);
    setAdmin(filtred)

}
return (
    <div id="addAccount">
        {!show
            ? <section>
                <h4>Admin accounts:</h4>
                { admin.map(x =>
                    <div key={x.email}>
                        <h4>{ x.name }</h4>
                        <h4>{ x.email }</h4>
                        <MdDelete onClick={deleteHandler} />
                    </div>
                ) }
                <h3 onClick={ () => setShow(true) }>Add another account</h3>
            </section>
            :
            <form action="" onSubmit={ submitHandler }>
                <input name="name" type="text" placeholder="Name" />
                <input name="email" type="text" placeholder="Email" />
                <div>
                    <button type="button" onClick={ () => setShow(false) }>Back</button>
                    <button >Submit</button>
                </div>
            </form>
        }
    </div>
);
}

export default AddAdmins;