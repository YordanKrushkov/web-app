import './index.scss';
import Header from '../../Components/Header/Header'
import HighligntsVideos from '../../Components/HiglightsVideos'
import Footer from '../../Components/Footer';
import { FaPlay } from "react-icons/fa";
import ReactPlayer from 'react-player'
import { useState, useEffect } from 'react'
const Hightlights = () => {
    const [play, setPlay] = useState(false);
    let [width,setWidth]=useState(window.innerWidth <800)
    const [id, setId] = useState('pouseMyClubVideo');
    const [source, setSource] = useState({
        url: "http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4",
        title: "Full match replay"
    })
    useEffect(() => {
        if (play) {
            setId("playMyClubVideo")
        } else {
            setId('pouseMyClubVideo')
        }
        console.log(id);
    }, [play]);

    const setVideo = (e) => {
        setSource({
            url: e.currentTarget.firstChild.src,
            title: e.currentTarget.lastChild.textContent
        })
    }
    return (
        <div id="highlightsContainer">
            <div id="highlightsWrapper">
                <Header title="Featured" secondTitle='Highlights' />
                <main>
                    <div>
                        <div className="clubInfo">
                            <p>A collection of the best footage from the Sport Stream archives.
                                We fish out the moments across all our sports that just have to be watched again.</p>
                            <p>Our highlight of the week feature is also shared to our social media channels.
                                If your club had a moment that deserves to be in the running for highlight of the week, be sure to tag us or message us on Instagram or Facebook to make sure we consider it!</p>
                            <p>All featured highlights are available to view on the <br id="mobBreak"/>Sport Stream app</p>
                        </div>

                    </div>
                    <div id="reversed">
                        <div id="highlightsVideo" className={ id }>
                            <div>
                                <ReactPlayer width="98%" height="95%" url={ source.url }
                                    playing={ play }
                                    onPause={ () => setPlay(false) }
                                    onSeek={ () => setPlay(true) }
                                    controls={ play }
                                    style={ { border: "none" } }
                                />
                                { !play && <FaPlay onClick={ () => setPlay(true) } /> }
                            </div>
                            <h5>{ source.title }</h5>
                        </div>
                        <div id="highlightsInfo">
                            <h1>Highlight of the week</h1>
                            <h4>18.07.21</h4>
                            <p>South Park FC's John Smith scores a goal that surely he'll never be able to top! A no brainer for highlight of the week</p>
                        </div>
                    </div>
                </main>
                <HighligntsVideos setVideo={ setVideo } />
            </div>
            {width && <Footer /> }
            {/* <div style={ { position: "relative" } }>
                <TeamLineUp />
                <SVG />
            </div> */}
        </div>
    );
}

export default Hightlights;