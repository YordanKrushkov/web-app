import getCookie from './cookies';
import baseUrl from './endpoints'

//Get User
// const getUser = async () => {

//     let promise = await fetch(getuser, {
//         method: 'GET',
//         headers: {
//             'Content-Type': 'application/json',
//             'Authorization': getCookie('x-auth-token'),
//         },
//     })
//     let res = await promise.json();
//     return res;
// };

//Verify User
const userVerify = async () => {
    let promise = await fetch(`${baseUrl}auth/verify`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': getCookie('x-auth-token')
        },
    })
    let res = await promise.json();
    return res;
};
const adminVerify = async () => {
    let promise = await fetch(`${baseUrl}auth/adminverify`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': getCookie('admin-auth-token')
        },
    })
    let res = await promise.json();
    return res;
};

export {
    userVerify,
    adminVerify
}